<#
  .SYNOPSIS
  This script [TBD]

  .DESCRIPTION
  This script [DESCRIPTION]

  .PARAMETER [First Param]
  First Param does [this]

  .INPUTS
  [TBD]
  
  .OUTPUTS
  [System.String containing the blah blah]
  
  .EXAMPLE
  [Example Description]
    PS> <[TBD]>

  .LINK
  TBD

  .NOTES
  Author:         Mike Owens, mikeowens (at) fastmail (dot) com
  Website:        https://michaelowens.me
  GitLab:         https://gitlab.com/qu13t0ne
  GitHub:         https://github.com/qu13t0ne
  Mastodon:       https://infosec.exchange/@qu13t0ne
  X (Twitter):    https://x.com/qu13t0ne
  Creation Date:  YYYY-MM-DD
  Modified Date:  
  Purpose/Change: Initial

#>

#region ----- Param --------------------
# SAMPLE PARAMETER BLOCK
Param(
    [Parameter(Mandatory)] [ValidateScript({Resolve-Path $_})] [String] $Path,
    [ValidateScript({Resolve-Path $_})] [String] $OutDirectory = '.'
)
#endregion ----- Param --------------------

Write-Verbose "Beginning $($MyInvocation.InvocationName)"
#region ----- Main --------------------

#YOUR CODE HERE

#endregion ----- Main --------------------
Write-Verbose "`n$($MyInvocation.InvocationName) is complete"

# //============= REFERENCES ===================\\
# \\============================================//

<# ----- References --------------------
- TBD
- https://learn.microsoft.com/en-us/powershell/scripting/developer/cmdlet/approved-verbs-for-windows-powershell-commands
#>
# eof