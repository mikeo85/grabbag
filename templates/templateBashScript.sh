#!/usr/bin/env bash
set -euo pipefail
############################################################
#
#  TITLE
#  ---------------------------------------------------------
#  Subtitle
#
#        Written by: Mike Owens
#        Email:      mikeowens (at) fastmail (dot) com
#        Website:    https://michaelowens.me
#        GitLab:     https://gitlab.com/qu13t0ne
#        GitHub:     https://github.com/qu13t0ne
#        Mastodon:   https://infosec.exchange/@qu13t0ne
#        X (Twitter):https://x.com/qu13t0ne
#
#  Description of script plus any instructions
#  
############################################################

# //========== BEGIN INPUT VALIDATION ==========\\

# Initialize variables for options and parameters
verbose=false
input_path=""
target_path=""

# Define a function to display the usage of the script
usage() {
  echo ""
  echo "Usage: $0 [-v] [<input_path> <target_path>] [-i <input_path>] [-t <target_path>] [-h]"
  echo "Options:"
  echo "  -v    Enable verbose mode"
  echo "  -i    Specify the input path"
  echo "  -t    Specify the target path"
  echo "  -h    Display this help message"
  echo ""
  exit 1
}

# Function for verbose logging
    # To use verbose mode, add `-v` option when running the script
    # To add verbose output, include a line in the script `log "Message"`
log() {
  if $verbose; then
    echo "$1"
  fi
}

# Use getopts for option parsing
while getopts ":vi:t:h" opt; do
  case $opt in
    v)
      verbose=true
      ;;
    i)
      input_path="$OPTARG"
      ;;
    t)
      target_path="$OPTARG"
      ;;
    h)
      usage
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      usage
      ;;
  esac
done

# Shift the positional parameters to exclude the parsed options
shift $((OPTIND-1))

# Automatically assign input and target paths from positional arguments
if [ -z "$input_path" ] && [ -z "$target_path" ] && [ $# -ge 2 ]; then
  input_path="$1"
  target_path="$2"
  shift 2
fi

# Check for required arguments
if [ -z "$input_path" ] || [ -z "$target_path" ]; then
  echo "Error: -i, -t options, or input and target paths are required."
  usage
fi

# \\=========== END INPUT VALIDATION ===========//

# //============================================\\
# \\============================================//

echo "Running $(basename "$0")..."
log "Verbose mode is enabled."
# //============ BEGIN SCRIPT BODY =============\\

# \\============= END SCRIPT BODY ==============//
echo "$(basename "$0") complete."
