#!/bin/bash
#########################################################
#  Merge .ics Calendar Files for Easy Import
#  [[ By: Mike Owens (GitLab/GitHub @ qu13t0ne) ]]
#  ---------------------------------------------------------
#  Reference:
#  - https://unix.stackexchange.com/questions/539600/merge-multiple-ics-calendar-files-into-one-file
#  - https://iaizzi.me/2021/02/10/how-to-merge-ics-files/
#########################################################

if [ "$1" != "" ];then
    dir="$1"
else
    dir="$PWD"
fi

if [ $(find $dir -iname "*.ics" | wc -l | xargs) -gt 0 ]; then
    mergefile="$dir/calendarMerge_$(date +"%Y%m%dT%H%M%S")"
    echo "BEGIN:VCALENDAR" >> $mergefile.temp;
    for file in $dir/*.ics; do 
        echo "Merging $file"
        cat "$file" | sed -e '$d' $1 | sed -e '1,/VEVENT/{/VEVENT/p;d;}' $2  >> $mergefile.temp
    done
    echo "END:VCALENDAR" >> $mergefile.temp;
    mv $mergefile.temp $mergefile.ics
    echo "Combined calendar file is $mergefile.ics"
else
    echo "Warning: No .ics files identified in directory $dir"
fi
