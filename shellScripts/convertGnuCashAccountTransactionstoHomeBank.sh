#!/bin/bash

# Check if the correct number of parameters are provided
if [ "$#" -ne 1 ]; then
	echo "Usage: $0 <input_file_path>"
	exit 1
fi

input_file="$1"

# Check if input file exists
if [ ! -f "$input_file" ]; then
	echo "Input file does not exist: $input_file"
	exit 1
fi

# Generate output file name with current datetime and source file name (without extension)
datetime=$(date +"%Y%m%dT%H%M%S")
input_filename=$(basename "$input_file")
input_filename_no_ext="${input_filename%.*}"
output_filename="${input_filename_no_ext}-Conversion-${datetime}.csv"
output_filepath="./${output_filename}"

# Write the header row to the output file
echo "date;paymode;info;payee;memo;amount;c;category;tags" >"$output_filepath"

# Read and process the input CSV, skipping the header row
tail -n +2 "$input_file" | while IFS=";" read -r date account_name number description full_category_path reconcile amount_with_sym amount_num rate_price; do
	# Remove commas from the amount field
	amount_clean=$(echo "$amount_num" | tr -d ',')

	# Map fields: date, description (info), full category path (memo), and cleaned amount
	echo "$date;;$description;;$full_category_path;$amount_clean;;;;" >>"$output_filepath"
done

echo "Conversion complete. Output saved to $output_filepath"
