# Create VSCode Snippet From Code Block

A [CyberChef](https://gchq.github.io/CyberChef/) recipe.

Input: The code to be converted to a snippet.

Output: A fully-formatted code snippet block to be inserted into a `Project_Snippets.code-snippets` file for VS Code use. Slashes, quotes, and dollar signs are properly escaped.

## The Recipe
```
Find_/_Replace({'option':'Regex','string':'\\n^$'},'',true,false,true,false)
Find_/_Replace({'option':'Regex','string':'\\"'},'\\\\"',true,false,true,false)
Find_/_Replace({'option':'Regex','string':'\\$'},'\\\\\\$',true,false,true,false)
Comment('The next step is optional and is empty by default. If the entire code block is indented (i.e. first line does not start at column 1), then enter the indent amount (spaces or tabs) in the \'Find\' block following the carrot \'^\'. This will normalize the code block to start at column 1. Indentations within the code block itself will be preserved.')
Find_/_Replace({'option':'Regex','string':'^'},'',true,false,true,false)
Find_/_Replace({'option':'Regex','string':'^(.*)$'},'\\t\\t\\t"$1"',true,false,true,false)
Find_/_Replace({'option':'Regex','string':'(^[\\s\\S]*$)'},'\\t\\t"body": [\\n$1\\n\\t\\t]',true,false,true,false)
Find_/_Replace({'option':'Regex','string':'(^[\\s\\S]*$)'},'\\t\\t"description": "",\\n$1',true,false,true,false)
Find_/_Replace({'option':'Regex','string':'(^[\\s\\S]*$)'},'\\t\\t"prefix": "",\\n$1',true,false,true,false)
Find_/_Replace({'option':'Regex','string':'(^[\\s\\S]*$)'},'\\t"[TITLE]": {\\n$1\\n\\t},',true,false,true,false)
```

## Example

### Input
```
$var1 = foreach ($var in $var2) {
  if ( $var.thing -eq "something" ) {
    Write-Host "$var is a something"
  }
}
```
### Output
```
	"[TITLE]": {
		"prefix": "",
		"description": "",
		"body": [
			"\\$var1 = foreach (\\$var in \\$var2) {"
			"  if ( \\$var.thing -eq \"something\" ) {"
			"    Write-Host \"\\$var is a something\""
			"  }"
			"}"
		]
	},
```