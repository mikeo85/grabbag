# CyberChef Recipes

This directory contains various 'recipes' for use in CyberChef.

Each recipe is a markdown file containing a description of the recipe purpose / use case and the recipe code. To use, open a CyberChef web app instance (either [the one hosted on GitHub](https://gchq.github.io/CyberChef/) or a self-hosted version) and paste the recipe code into the *Recipe* section of the app.

## About CyberChef

**Summary:**
> The Cyber Swiss Army Knife - a web app for encryption, encoding, compression and data analysis

**Description:**
> CyberChef is a simple, intuitive web app for carrying out all manner of "cyber" operations within a web browser. These operations include simple encoding like XOR and Base64, more complex encryption like AES, DES and Blowfish, creating binary and hexdumps, compression and decompression of data, calculating hashes and checksums, IPv6 and X.509 parsing, changing character encodings, and much more.
>
> The tool is designed to enable both technical and non-technical analysts to manipulate data in complex ways without having to deal with complex tools or algorithms. It was conceived, designed, built and incrementally improved by an analyst in their 10% innovation time over several years.

**GitHub Repo:** https://github.com/gchq/CyberChef

**Hosted Web App:** https://gchq.github.io/CyberChef