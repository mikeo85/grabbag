#!/bin/bash
#########################################################
#  Fix where the linked H1 header doesn't match the filename in Obsidian Notes
#  
#        Written by: Mike Owens
#        Email:      mikeowens (at) fastmail (dot) com
#        Website:    https://michaelowens.me
#        GitLab:     https://gitlab.com/qu13t0ne
#        GitHub:     https://github.com/qu13t0ne
#        Mastodon:   https://infosec.exchange/@qu13t0ne
#        X (Twitter):https://x.com/qu13t0ne
#
#   Instructions:
#   - One input required: The file path to traverse and check
#########################################################

filepath="$1"
filelist="$(find "$1" -iname '*.md')"
for file in "$filelist";do
    ext=$(basename "$file" | cut -f 2 -d '.')
    # if [ "$ext" == 'md' ];then
        echo "$ext :: $file"
    # fi
done